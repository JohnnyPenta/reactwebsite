import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import useStyle from "./styles";
import { withStyles } from "@material-ui/core/styles";

const StyledTableCell = withStyles((theme) => ({
   head: {
      backgroundColor: "#37474f",
      color: theme.palette.common.white,
   },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
   root: {
      "&:nth-of-type(odd)": {
         backgroundColor: theme.palette.action.hover,
      },
   },
}))(TableRow);

function createData(name, name2, name3, name4, name5) {
   return { name, name2, name3, name4, name5 };
}

const rows = [
   createData("Lorem Ipsum", 159, 6.0, 24, 4.0),
   createData("Lorem Ipsum", 237, 9.0, 37, 4.3),
   createData("Lorem Ipsum", 262, 16.0, 24, 6.0),
   createData("Lorem Ipsum", 305, 3.7, 67, 4.3),
   createData("Lorem Ipsum", 356, 16.0, 49, 3.9),
];

const Tables = () => {
   const classes = useStyle();

   return (
      <TableContainer component={Paper}>
         <Table className={classes.table} aria-label="customized table">
            <TableHead>
               <TableRow>
                  <StyledTableCell>Lorem Ipsum</StyledTableCell>
                  <StyledTableCell align="right">Lorem Ipsum</StyledTableCell>
                  <StyledTableCell align="right">Lorem Ipsum)</StyledTableCell>
                  <StyledTableCell align="right">Lorem Ipsum</StyledTableCell>
                  <StyledTableCell align="right">Lorem Ipsum</StyledTableCell>
               </TableRow>
            </TableHead>
            <TableBody>
               {rows.map((row) => (
                  <StyledTableRow key={row.name}>
                     <StyledTableCell component="th" scope="row">
                        {row.name}
                     </StyledTableCell>
                     <StyledTableCell align="right">{row.name2}</StyledTableCell>
                     <StyledTableCell align="right">{row.name3}</StyledTableCell>
                     <StyledTableCell align="right">{row.name4}</StyledTableCell>
                     <StyledTableCell align="right">{row.name5}</StyledTableCell>
                  </StyledTableRow>
               ))}
            </TableBody>
         </Table>
      </TableContainer>
   );
};

export default Tables;
