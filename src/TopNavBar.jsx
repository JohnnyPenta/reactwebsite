import { Avatar, Button, IconButton, Menu, MenuItem, ThemeProvider } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import useStyle from "./styles";
import theme from "./themes";

const TopNavBar = () => {
   const classes = useStyle();

   const [anchorEl, setAnchorEl] = React.useState(null);

   const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
   };
   const handleClose = () => {
      setAnchorEl(null);
   };

   return (
      <ThemeProvider theme={theme}>
         <div>
            <AppBar position="absolute" color="primary">
               <Toolbar>
                  <IconButton className={classes.topNavMenuIcon}>
                     <MenuIcon htmlColor="white" />
                  </IconButton>
                  <Typography variant="h6">Hello World!</Typography>

                  <div className={classes.topNavMenu}>
                     <Button
                        aria-controls="simple-menu"
                        aria-haspopup="true"
                        onClick={handleClick}
                        variant="contained"
                        size="small"
                     >
                        Lorem Ipsum
                     </Button>
                     <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                     >
                        <MenuItem onClick={handleClose}>Profile</MenuItem>
                        <MenuItem onClick={handleClose}>My account</MenuItem>
                        <MenuItem onClick={handleClose}>Logout</MenuItem>
                     </Menu>
                  </div>
                  <Avatar />
                  <IconButton>
                     <ExitToAppIcon htmlColor="white" />
                  </IconButton>
               </Toolbar>
            </AppBar>
         </div>
      </ThemeProvider>
   );
};

export default TopNavBar;
