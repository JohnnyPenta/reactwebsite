import React from "react";
import Typography from "@material-ui/core/Typography";
import useStyle from "./styles";
import theme from "./themes";
import { ThemeProvider } from "@material-ui/core";

const Footer = () => {
   const classes = useStyle();

   return (
      <ThemeProvider theme={theme}>
         <div className={classes.footerDiv}>
            <Typography variant="h5" align="center" color="secondary">
               Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique maxime vel in libero nostrum sit
               laborum ea repudiandae id sed.
            </Typography>
            <Typography variant="h6" align="center" color="textPrimary">
               Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique maxime vel in libero nostrum sit
               laborum ea repudiandae id sed.
            </Typography>
         </div>
      </ThemeProvider>
   );
};

export default Footer;
