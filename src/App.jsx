import "./App.css";
import Footer from "./Footer";
import MainContent from "./MainContent";
import TopNavBar from "./TopNavBar";

const App = () => {
   return (
      <div className="App">
         <TopNavBar />
         <MainContent />
         <Footer />
      </div>
   );
};

export default App;
