import { ThemeProvider } from "@material-ui/core";
import React from "react";
import useStyle from "./styles";
import theme from "./themes";
import LeftNavMainContent from "./LeftNavMainContent";
import DriversToolsDiv from "./DriversToolsDiv";
import TableMainContent from "./TableMainContent";
import LittleTopNavMainContent from "./LittleTopNavMainContent";

const MainContent = () => {
   const classes = useStyle();

   return (
      <ThemeProvider theme={theme}>
         <LittleTopNavMainContent />
         <div className={classes.flexDiv}>
            <LeftNavMainContent />
            <div className={classes.colmnDiv}>
               <DriversToolsDiv />
               <TableMainContent />
            </div>
         </div>
      </ThemeProvider>
   );
};

export default MainContent;
