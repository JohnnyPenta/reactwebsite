import CloseIcon from "@material-ui/icons/Close";
import LoopIcon from "@material-ui/icons/Loop";
import HeightIcon from "@material-ui/icons/Height";
import BuildIcon from "@material-ui/icons/Build";
import useStyle from "./styles";
import { Button, Grid, Typography, Paper } from "@material-ui/core";

const DriversToolsDiv = () => {
   const classes = useStyle();
   return (
      <div className={classes.driverTools}>
         <Paper variant="elevation" elevation={8}>
            <div className={classes.driverTools}>
               <div className={classes.driverToolsInside}>
                  <Grid>
                     <Typography variant="h6" color="error">
                        Lorem Ipsum
                     </Typography>
                  </Grid>
                  <Grid container alignItems="flex-start" justify="flex-end" xs>
                     <LoopIcon htmlColor="#b09fa5" className={classes.icon} />
                     <BuildIcon htmlColor="#b09fa5" className={classes.icon} />
                     <HeightIcon htmlColor="#b09fa5" className={classes.icon} />
                     <CloseIcon htmlColor="#b09fa5" className={classes.icon} />
                  </Grid>
               </div>
               <div className={classes.driverToolsAllButtons}>
                  <div className={classes.driverToolsButtonDiv1}>
                     <Button variant="contained" color="primary" size="large">
                        Lorem Ipsum
                     </Button>
                  </div>
                  <div className={classes.driverToolsButtonDiv2}>
                     <Button variant="contained" className={classes.btn} size="large">
                        Lorem Ipsum
                     </Button>
                     <Button variant="contained" color="secondary" size="large">
                        Lorem Ipsum
                     </Button>
                  </div>
               </div>
            </div>
         </Paper>
      </div>
   );
};

export default DriversToolsDiv;
