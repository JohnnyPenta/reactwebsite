import { createMuiTheme } from "@material-ui/core";
import { blueGrey, grey } from "@material-ui/core/colors";

const theme = createMuiTheme({
   palette: {
      primary: {
         main: blueGrey[800],
      },
      secondary: {
         main: grey[50],
      },
   },
});

export default theme;
