import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles({
   topNavMenu: {
      flexGrow: "3",
      marginLeft: "2rem",
   },
   breadcrumbsDiv: {
      paddingTop: "5rem",
      marginLeft: "20rem",
   },
   mainContentLeftList: {
      width: "20rem",
      height: "20rem",
      padding: "0 2rem",
   },
   flexDiv: {
      display: "flex",
      paddingTop: "1rem",
   },
   driverTools: {
      height: "9rem",
      width: "98rem",
   },
   driverToolsInside: {
      display: "flex",
      padding: "1rem",
      flexGrow: "1",
   },
   icon: {
      padding: "0.25rem",
   },
   driverToolsAllButtons: {
      display: "flex",
      padding: "1rem",
   },
   driverToolsButtonDiv1: {
      flexGrow: "2",
   },
   driverToolsButtonDiv2: {
      flexGrow: "3",
   },
   btn: {
      marginRight: "1rem",
   },
   colmnDiv: {
      display: "flex",
      flexDirection: "column",
   },
   tableDiv: {
      marginTop: "1rem",
   },
   tableTitle: {
      padding: "1rem",
   },
   table: {
      minWidth: "700px",
   },
   footerDiv: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
      marginTop: "6rem",
      height: "10.95rem",
      width: "auto",
      backgroundColor: "#37474f",
   },
});

export default useStyle;
