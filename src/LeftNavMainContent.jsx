import ListItems from "./ListItems";
import PersonIcon from "@material-ui/icons/Person";
import DraftsIcon from "@material-ui/icons/Drafts";
import DateRangeIcon from "@material-ui/icons/DateRange";
import LocalGroceryStoreIcon from "@material-ui/icons/LocalGroceryStore";
import AddAlertIcon from "@material-ui/icons/AddAlert";
import { List, Paper } from "@material-ui/core";
import useStyle from "./styles";

const LeftNavMainContent = () => {
   const classes = useStyle();
   return (
      <div className={classes.mainContentLeftList}>
         <Paper variant="elevation" elevation={2}>
            <List>
               <ListItems avatar={<PersonIcon htmlColor="black" />} value="Lorem Ipsum" />
               <ListItems avatar={<DraftsIcon htmlColor="black" />} value="Lorem Ipsum" />
               <ListItems avatar={<DateRangeIcon htmlColor="black" />} value="Lorem Ipsum" />
               <ListItems avatar={<LocalGroceryStoreIcon htmlColor="black" />} value="Lorem Ipsum" />
               <ListItems avatar={<AddAlertIcon htmlColor="black" />} value="Lorem Ipsum" />
            </List>
         </Paper>
      </div>
   );
};

export default LeftNavMainContent;
