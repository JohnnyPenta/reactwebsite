import { Link, Breadcrumbs } from "@material-ui/core";
import useStyle from "./styles";

const LittleTopNavMainContent = () => {
   const classes = useStyle();
   return (
      <div className={classes.breadcrumbsDiv}>
         <Breadcrumbs separator="-">
            <Link color="inherit" href="#">
               Lorem
            </Link>
            <Link color="inherit" href="#">
               Ipsum
            </Link>
            <Link color="inherit" href="#">
               Dolore
            </Link>
         </Breadcrumbs>
      </div>
   );
};

export default LittleTopNavMainContent;
