import { Avatar, ListItem, ListItemAvatar, ListItemText } from "@material-ui/core";
import React from "react";

const ListItems = (props) => {
   return (
      <ListItem button>
         <ListItemAvatar>
            <Avatar>{props.avatar}</Avatar>
         </ListItemAvatar>
         <ListItemText>{props.value}</ListItemText>
      </ListItem>
   );
};

export default ListItems;
