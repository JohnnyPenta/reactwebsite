import DeleteIcon from "@material-ui/icons/Delete";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import UndoIcon from "@material-ui/icons/Undo";
import SettingsOverscanIcon from "@material-ui/icons/SettingsOverscan";
import Tables from "./Tables";
import useStyle from "./styles";
import { Grid, Paper, Typography } from "@material-ui/core";

const TableMainContent = () => {
   const classes = useStyle();

   return (
      <div className={classes.tableDiv}>
         <Paper>
            <div className={classes.tableTitle}>
               <Grid container direction="row">
                  <Grid container xs>
                     <Typography variant="h5" color="primary">
                        Lorem ipsum dolor sit amet.
                     </Typography>
                  </Grid>
                  <Grid container justify="flex-end" spacing={1} xs>
                     <Grid item>
                        <UndoIcon htmlColor="green" />
                     </Grid>
                     <Grid item>
                        <CloudUploadIcon htmlColor="royalblue" />
                     </Grid>
                     <Grid item>
                        <DeleteIcon htmlColor="red" />
                     </Grid>
                     <Grid item>
                        <SettingsOverscanIcon htmlColor="purple" />
                     </Grid>
                  </Grid>
               </Grid>
            </div>
            <div className={classes.dtable}>
               <Tables />
            </div>
         </Paper>
      </div>
   );
};

export default TableMainContent;
